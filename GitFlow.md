# Git Flow
**Git Flow** - is an alternative Git branching model that uses work branches and multiple master branches.  
![](https://gitlab.com/portfolio.eldar/ios-documentation/-/raw/main/GitFlow.png)
**Main branches** such as **dev** and **main**.

**main** - Revises code to **dev** after successful testing, it will be merged into **main** branch.

**dev** - Responsible for the code in the working branches, it contains the development code and, after the completion of work on the code, it is transferred to testing.

**work branch** - Reply to the code you are currently working on.
