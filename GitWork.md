# Git Work - На случии работы с Git Flow

### Начало на новой залачи (Через XCode)
В первую очередь вам нужно склонить репозиторий `git clone https://gitlab.com/example` или скачать с помошью **IDE**.

Когда вам поступила новоя задача вам в первую очередь нужно сделать **Fetch Changes** 3 раза (Для полного обновдение)

![](https://gitlab.com/portfolio.eldar/ios-documentation/-/raw/main/XCodeGitWork/Screen%20Shot%202022-08-22%20at%2014.37.05.png)

После нужно зайти в ui git xcode он находиьться рядом с файлами проекта **Убедитесь что вы нажали на Repositpries**

![](https://gitlab.com/portfolio.eldar/ios-documentation/-/raw/main/XCodeGitWork/Screen%20Shot%202022-08-22%20at%2014.39.46.png)

После создать новую рабочую вертку из **dev**

![](https://gitlab.com/portfolio.eldar/ios-documentation/-/raw/main/XCodeGitWork/Screen%20Shot%202022-08-22%20at%2014.37.15.png)

Ввести ее новое имя вашей рабочей ветке

![](https://gitlab.com/portfolio.eldar/ios-documentation/-/raw/main/XCodeGitWork/Screen%20Shot%202022-08-22%20at%2014.37.34.png)

После вам необходимо перейти на эту ветку с помошью **СheckOut**

![](https://gitlab.com/portfolio.eldar/ios-documentation/-/raw/main/XCodeGitWork/Screen%20Shot%202022-08-22%20at%2014.38.01.png)

Вам выйдет сплываюшее окно где вы должны нажать на **СheckOut**

![](https://gitlab.com/portfolio.eldar/ios-documentation/-/raw/main/XCodeGitWork/Screen%20Shot%202022-08-22%20at%2014.38.07.png)

После вы должны убедиться что вы находитесь на той ветке которую вы создали. На примере видно что **checkOut** прошел успешно так как **current** ветка это рабочая ветка которую мы создали

![](https://gitlab.com/portfolio.eldar/ios-documentation/-/raw/main/XCodeGitWork/Screen%20Shot%202022-08-22%20at%2014.38.16.png)

После чего вы можете писать код делать фичи фиксить баги после того как вы закончили работать вам необходимо закомитить ваш код

![](https://gitlab.com/portfolio.eldar/ios-documentation/-/raw/main/XCodeGitWork/Screen%20Shot%202022-08-22%20at%2014.38.42.png)

После у вам откроеться окно для просмотра изменений и описание коментария вы должны кратко описать что делаи в данном коде. После нажимаем на **commit**

![](https://gitlab.com/portfolio.eldar/ios-documentation/-/raw/main/XCodeGitWork/Screen%20Shot%202022-08-22%20at%2014.39.00.png)

После того как вы закамитили изменения необходима запушить комиты создав ветка

![](https://gitlab.com/portfolio.eldar/ios-documentation/-/raw/main/XCodeGitWork/Screen%20Shot%202022-08-22%20at%2014.39.27.png)

После вам необходима Создать ветку

![](https://gitlab.com/portfolio.eldar/ios-documentation/-/raw/main/XCodeGitWork/Screen%20Shot%202022-08-22%20at%2014.39.32.png)

После того как вы создали ветку вам необходимо зайти в гитлаб 

![](https://gitlab.com/portfolio.eldar/ios-documentation/-/raw/main/XCodeGitWork/Screen%20Shot%202022-08-22%20at%2014.40.07.png)

Зайти в раздел **merge request** и нажать на кнопку **New Merge Request**

![](https://gitlab.com/portfolio.eldar/ios-documentation/-/raw/main/XCodeGitWork/Screen%20Shot%202022-08-22%20at%2014.40.15.png)

После указать вашу ветку и ветку **dev** в **Merge Request**

![](https://gitlab.com/portfolio.eldar/ios-documentation/-/raw/main/XCodeGitWork/Screen%20Shot%202022-08-22%20at%2014.40.30.png)

После заполнить **Merge Request**

![](https://gitlab.com/portfolio.eldar/ios-documentation/-/raw/main/XCodeGitWork/Screen%20Shot%202022-08-22%20at%2014.40.39.png)

После того как вы заполнили MR вы должны отправить мне ссылаку на MR
